# dotfiles

## Install zsh

zsh + oh-my-zsh + zsh-autosuggestions + zsh-syntax-highlighting
### macOS
```sh
brew install zsh
sudo chsh -s /usr/local/bin/zsh $USER
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

### Linux
```sh
sudo apt install zsh
sudo chsh -s /bin/zsh $USER
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

then add `activate-venv` to .zshrc
```sh
echo 'alias activate-venv="source venv/bin/activate"' >> ~/.zshrc
```

Add plugin `zsh-syntax-highlighting` and `zsh-autosuggestions` to plugins on .zshrc

## Neovim

use [nvChad](https://nvchad.netlify.app/getting-started/setup/)

```sh
git clone https://github.com/NvChad/NvChad ~/.config/nvim
nvim +'hi NormalFloat guibg=#1e222a' +PackerSync
```

## postgresql

install postgresql, createuser $USER as superuser, create db. Use odoo.conf file and change db_name when change project.

## Terminal
### macOS
using iTerm2 + profile from [dotfiles](https://github.com/rockavoldy/dotfiles)

change font from preferences iTerm2

### Linux
using builtin terminal (pantheon, gnome, kterm) then use [gogh](https://mayccoll.github.io/Gogh/) with colorscheme AyuMirage(13)

for pantheon, change font with command
```sh
gsettings set io.elementary.terminal.settings font "FiraCode"
```
and you can see available font with command
```sh
fc-list | cut -f2 -d: | sort -u
```

use FiraCode font if possible
